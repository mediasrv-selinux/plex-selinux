#!/bin/bash

# Fix bad file modes for image and text/xml files in /usr/lib/plexmediaserver

/usr/bin/find . -name '*.png' -exec /usr/bin/chmod -x {} \;
/usr/bin/find . -name '*.jpg' -exec /usr/bin/chmod -x {} \;
/usr/bin/find . -name '*.xml' -exec /usr/bin/chmod -x {} \;