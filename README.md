# SELinux policy module for Plex Media Server

This is an SELinux policy module that defines a domain to confine Plex Media
Server and associated processes. By default, Plex Media Server runs in the
`unconfined_service_t` domain. This gives Plex Media Server far more access to the system
than it needs to do its job. Installing this policy module will confine Plex
Media Server in its own domain with a tailored set of access controls.

This policy module should not interfere with Plex's normal operation except
where noted (i.e Web UI file browser). If you find something is not working
correctly please open an issue.

Bug reports and patches are welcome.

### Support Status

Supported Platforms: CentOS 7, Fedora 29-31

Supported Plex Media Server Version: plexmediaserver-1.15.1.710+

### Current Release Notes

The Makefile assumes that the plex_port_t is already defined, this is not true the first time you install the policy.
If you get "ValueError: Port tcp/32*** is not defined then run the semanage -m commands in the Makefile as follows:

    semanage port -a -t plex_port_t -p tcp 32400
    semanage port -a -t plex_dlna_port_t -p tcp 32469

The tmp dir for Transcoding is assumed to be /var/lib/plexmediaserver/Library/Transcode and it is set to the `plex_tmp_t` context.
You must set it to this directory within Plex or modify the policy.

## Building

### Requirements

In order to build and install this policy module you will need the following
packages installed:

* make
* selinux-policy
* selinux-policy-devel
* policycoreutils-python for Red Hat/CentOS <= 7 and Fedora <=31
* python3-policycoreutils for Red Hat CentOS 8+ and Fedora 32+

### Build

	$ make

### Install

Installs the policy package and relabels Plex Media Server files to new file
contexts.

(as root)

	# make install


### Uninstall

Uninstalls the policy module from the system and relabels Plex Media Server
files to the default file contexts.

(as root)

	# make uninstall


## Policy Details

### Domain

SELinux uses domains to label processes. The domain of a confined process
defines what access controls the process is subjected to.

The security context of a process can be inspected by adding the `-Z` option to
the `ps` command

	$ ps -eZ | grep plex_t
	unconfined_u:system_r:plex_t:s0  7174 ?        00:00:04 Plex Media Serv
	unconfined_u:system_r:plex_t:s0  7281 ?        00:00:16 Plex DLNA Serve
	unconfined_u:system_r:plex_t:s0  7865 ?        00:00:39 python

This policy module defines the domain `plex_t` for Plex Media Server processes.


### File Contexts

SELinux requires that files be labeled with a security context so it knows what
access controls to apply. This security context is usually stored in an extended
attribute on the file.

The security context of a file can be inspected by using the `-Z` option with
the `ls` command.

	$ ls -Z /var/lib/plexmediaserver
	drwxr-xr-x. plex plex unconfined_u:object_r:plex_var_lib_t:s0 Library

A file's security context can be changed temporarily using `chcon`. These
changes will not survive a relabeling.

	$ chcon -R /usr/share/my_media_library

A file's security context can be changed permanently by using `semanage fcontext`
to define the default security context for a given set of files and then using
`restorecon` to apply those labels.

	$ semanage fcontext -a -t plex_content_t "/usr/share/my_media_library(/.*)?"
	$ restorecon -v -R /usr/share/my_media_library

This policy module defines the following file contexts:

##### plex\_content\_t and plex\_content\_rw\_t

These file contexts are for labeling media files and directories that processes
confined by `plex_t` should be allowed to read (`plex_content_t`) or manage
(`plex_content_rw_t`).
<br />
<br />
Always use `plex_content_rw_t` for DVR libraries.
<br />
<br />
##### plex\_etc\_t

Plex Media Server configuration files.
<br />
<br />

##### plex\_initrc\_exec\_t

Plex Media Server init.d scripts.
<br />
<br />

##### plex\_exec\_t

Plex Media Server executables. Files labeled with this context will create
processes in the `plex_t` domain when called from certain domains
(notably `init_t` and `initrc_t`). Generally a transition to `plex_t` will not
occur if the calling process is `unconfined_t` (the standard user context in
the targeted policy). This means if a user calls the executable from a shell
the resulting process **will not** be confined.
<br />
<br />

##### plex\_var\_lib\_t

Plex Media Server `/var/lib` files. Processes confined by `plex_t` can manage
files and directories in this domain.
<br />
<br />

##### plex\_tmp\_t and plex\_tmpfs\_t

Plex Media Server temporary files and objects. Processes confined by `plex_t`
can manage files and directories in this domain.
<br />
<br />

##### Executable contexts

Processes confined by `plex_t` have permission to execute files in the
following security contexts:

* `plex_exec_t`
* `plex_var_lib_t`
* `plex_tmp_t`
* `plex_tmpfs_t`
* `rsync_exec_t`
* `bin_t`
* `shell_exec_t`

Processes created from executables in these contexts will be confined by `plex_t`.
<br />
<br />

##### Shared contexts

Processes confined by `plex_t` have read access to the following
shared file contexts:

* `public_content_t`
* `public_content_rw_t`

This is useful if you want label media files to be readable by processes in
domains other than `plex_t` (Apache, NFS, Samba, FTP, etc)
<br />
<br />

### Networking

Processes confined by `plex_t` have the following network capabilities:

* May bind TCP sockets to `plex_port_t`
* May connect TCP sockets to `port_t`
* May connect TCP sockets to `http_port_t`
* May connect TCP sockets to `rtp_media_port_t`
* May connect TCP sockets to `postgrey_port_t`
* May bind UDP sockets to `port_t`
* May send and receive UDP packets to/from all ports.
* May send log messages to syslog/journald

The `rtp_media_port_t` capability is used for HDHomerun TV tuners (and others?). The port
is defined in system polices so access must be specifically granted to it.

The `postgrey_port_t` is newly defined in the base policy (~2019 ish) at port 60000, the plex DLNA server binds
to it as "free high port". A better solution is needed, such as defining more rigidly the
ports the DLNA service may use.

### Booleans

SELinux booleans can be used to adjust the access controls enforced for
`plex_t`. Default settings (off) result in tightest security. Enabling a boolean
will loosen access controls and allow `plex_t` confined processes more access
to the system.

The status of booleans can be inspected using `getsebool`

	$ getsebool -a | grep plex
	plex_access_all_ro --> off
	plex_access_all_rw --> off
	plex_access_home_dirs_rw --> off
	plex_access_sys_cert_ro --> off
	plex_anon_write --> off
	plex_list_all_dirs --> off
	plex_mediasrv_write --> on
	plex_use_sonarr --> off
	plex_use_radarr --> off
	plex_use_bonjour --> off
	plex_use_dlna -- on

A boolean can be turned on or off using `setsebool`

(as root)

`# setsebool allow_plex_list_all_dirs on`

A `-P` option can be passed to `setsebool` that makes the setting persist
across reboots.

This policy modules defines the following booleans:

##### plex\_access\_all\_ro

Allow processes contained by `plex_t` to read all files and directories.
<br />
<br />
Defaults to **off**. Should not be needed or used.
<br />
<br />

##### plex\_access\_all\_rw

Allow processes contained by `plex_t` to manage (read, write, create, delete)
all files and directories.
<br />
<br />
Defaults to **off** and should remain off. If you need this you are doing it wrong!
<br />
<br />

##### plex\_list\_all\_dirs

Allow processes contained by `plex_t` to list (search and read) all directories.

Note that this boolean will allow the directory browser in Plex's Web UI to work
correctly. Attempting to use the directory browser without enabling this boolean
will cause a large number of AVC denials to be logged. If this boolean is off,
directory paths should be typed in the path bar instead of browsed for.
<br />
Rather than use this it is recommended that you set the appropriate file contexts first
and type the path directly rather than using the browser. If used it should never be
used with the -P flag, and disabled when configuration is finished.
<br />
<br />
Defaults to **off**
<br />
<br />

##### plex\_anon\_write

Allow processes contained by `plex_t` to manage (read, write, create, delete)
files and directories labeled with the `public_content_rw_t` context.
<br />
<br />
Defaults to **off**
<br />
<br />

##### plex\_access\_home\_dirs\_rw

Allow processes contained by `plex_t` to manage (read, write, create, delete)
files and directories in user home directories.
<br />
<br />
Defaults to **off**
<br />
<br />

##### plex\_sys\_certs\_ro

Allows processes contained by `plex_t` to read cert\_t labeled files, normally this
is the system wide TLS certificates stored in /etc/pki/
<br />
<br />
This defaults to **on** so that the SSL certs for plex may be stored in the default locations.
<br />
<br />

##### plex\_use\_bonjour

Allows processes contained by `plex_t` to connect to `howl_port_t` to announce
itself via bonjour. This is an obsolete setting in plex and this setting exists
only for compatibility.
<br />
<br />
This defaults to **off**.
<br />
<br />

##### plex\_use\_dlna

Allows processes contained by `plex_t` to connect to `ssdp_port_t` to announce
itself via DLNA/SSDP.
<br />
<br />
This defaults to **on**.
<br />
<br />

### Optional Booleans

These values are only present if the applications and selinux polices from them are installed
on the system.

##### plex\_connect\_sonarr

Allows processes contained by `plex_t` to connect to `sonarr_port_t` if the application
and matching policy are installed.
<br />
<br />
This defaults to **off**.
<br />
<br />

##### plex\_connect\_radarr

Allows processes contained by `plex_t` to connect to `radarr_port_t` if the application
and matching policy are installed.
<br />
<br />
This defaults to **off**.
<br />
<br />


##### plex\_use\_mediasrv

Allows processes contained by `plex_t` to use the Mediaserver Framework shared contexts. This
allows for a shared file context between multiple media downloading and sharing applications
without using the `public_content_rw_t` context.
<br />
<br />
This defaults to **off**.
<br />
<br />

---
