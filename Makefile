include /usr/share/selinux/devel/Makefile

define assign-plex-ports
echo "Assigning Plex ports..."
semanage port -m -t plex_port_t -p tcp 32400
semanage port -m -t plex_dlna_port_t -p tcp 32469
endef

define remove-plex-ports
echo "Removing Plex ports..."
semanage port -d -t plex_port_t -p tcp 32400
semanage port -d -t plex_dlna_port_t -p tcp 32469
endef

define relabel-plex-files
echo "Relabeling files..."
restorecon /etc/sysconfig/PlexMediaServer
restorecon -DR /var/lib/plexmediaserver
restorecon -DR /usr/lib/plexmediaserver
endef

.PHONY: install uninstall

install:
	semodule -v -i plex.pp
	$(assign-plex-ports)
	$(relabel-plex-files)

uninstall:
	$(remove-plex-ports)
	semodule -v -r plex
	$(relabel-plex-files)	

